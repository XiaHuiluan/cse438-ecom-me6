package com.example.cse438.studio6.model

import com.google.firebase.Timestamp
import java.io.Serializable

class Review(): Serializable {
    private var body: String = ""
    private var isAnonymous: Boolean = false
    private var userId: String = ""
    private var username: String = ""
    private lateinit var date: Timestamp

    constructor(body: String, isAnonymous: Boolean, userId: String, username: String, date: Timestamp): this() {
        this.body = body
        this.isAnonymous = isAnonymous
        this.userId = userId
        this.username = username
        this.date = date
    }

    fun getBody(): String {
        return this.body
    }

    fun getIsAnonymous(): Boolean {
        return this.isAnonymous
    }

//    fun getUserId(): String {
//        return this.userId
//    }

    fun getUsername(): String {
        return this.username
    }

//    fun getDate(): Timestamp {
//        return date
//    }
}